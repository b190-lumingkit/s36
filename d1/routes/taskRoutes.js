// contains all endpoints for our application
// app.js/index.js should only be concerned with information about the server
// we have to use the Router method inside express
const express = require("express");
// access HTTP method middlewares that makes it easier to create routing system
const router = express.Router();

// import "routes/taskController.js"
const taskController = require("../controllers/taskController.js")

// route to get all tasks
/* 
    get request to be sent at localhost:3000/tasks
*/
router.get("/", (req, res) =>
{
    // since the controller did not send any response, the task falls to the routes to send a response that is received from the controllers and send it to the frontend
    taskController.getAllTasks()
    .then(resultFromController => res.send(resultFromController))
});

router.post("/", (req, res) => 
{
    taskController.createTask(req.body)
    .then(resultFromController => res.send(resultFromController))
});

// route for deleting tasks
router.delete("/:id", (req, res) =>
{
    taskController.deleteTask(req.params.id)
    .then(result => res.send(result));
});

// route for updating a task
router.put("/:id", (req, res) =>
{
    taskController.updateTask(req.params.id, req.body)
    .then(result => res.send(result));
});

// SECTION - ACTIVITY

// get specific task
router.get("/:id", (req, res) =>
{
    taskController.getTask(req.params.id)
    .then(dataFromController => res.send(dataFromController));
})

// change task status
router.put("/:id/complete", (req, res) => 
{
    taskController.updateStatus(req.params.id, req.body.status)
    .then(dataFromController => res.send(dataFromController));
});

// modules.exports - export the file where it is inserted to be used by other files such as app.js/index.js
module.exports = router;