// contains functions and business logic of our express js applications
// all operations it can do will be placed in this file
// allows us to use the contents of task.js inside the "models" folder
const express = require("express");
const Task = require("../models/task.js");

// defines the functions to be used in the "taskRoutes.js"
module.exports.getAllTasks = () => {
    return Task.find({}).then(result => {
        return result;
    });
};

module.exports.createTask = (reqBody) => {
    // check if task already exists
    return Task.findOne({name: reqBody.name}).then((result, error) =>
    {
        if (error)
        {
            console.log(error);
            return false;
        }
        else if (result !== null && result.name === reqBody.name)
        {
            return `Task already exists.`
        }
        else
        {
            let newTask = new Task({
                name: reqBody.name
            });
            return newTask.save().then((task, err) =>
            {
                if (err) 
                {
                    console.log(err);
                    return false;
                } 
                else 
                {
                    return task;
                }
            });
        }
    })
};

module.exports.deleteTask = (reqParams) => {
    console.log(reqParams);
    return Task.findByIdAndRemove({_id: reqParams})
    .then((task, error) => 
    {
        if (error) 
        {
            console.log(error);
            return false;
        } 
        else 
        {
            return task;
        }
    });
};

module.exports.updateTask = (taskId, newContent) => 
{
    return Task.findById(taskId).then((result, error) =>
    {
        if (error) 
        {
            console.log(error);
            return false;
        } 
        else 
        {
            result.name = newContent.name;
            return result.save()
            .then((updatedTask, error) =>
            {
                if (error) {
                    console.log(error);
                    return false;
                }
                else 
                {
                    return updatedTask;
                }
            })
        }
    })
};


// SECTION - ACTIVITY

// get a specific task
module.exports.getTask = (taskId) =>
{
    return Task.findById(taskId).then((result, error) =>
    {
        if (error) 
        {
            console.log(error);
            return false;
        } 
        else 
        {
            return result;
        }
    });
};

// change task status
module.exports.updateStatus = (taskId, newStatus) =>
{
    return Task.findById(taskId).then((result, error) =>
    {
        if (error) 
        {
            console.log(error);
            return false;
        } 
        else 
        {
            result.status = newStatus;
            return result.save()
            .then((updatedTask, error) => 
            {
                if (error) 
                {
                    console.log(error);
                    return false;
                } 
                else 
                {
                    return updatedTask;
                }
            })
        }
    });
}