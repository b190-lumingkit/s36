// import express and mongoose
const express = require("express");
const mongoose = require("mongoose");

// import "routes/taskRoutes.js"
const taskRoutes = require("./routes/taskRoutes.js");

// Server setup
const app = express();
const port = 3000;

// MongoDB Connection
mongoose.connect("mongodb+srv://jaycee24:brtMms9x3zndixW@wdc028-course-booking.8oezu.mongodb.net/b190-to-do?retryWrites=true&w=majority",
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
);
let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log(`We're connected to the database.`));

app.use(express.json());
app.use(express.urlencoded({extended:true}));

// create a route to locahost:3000/tasks
app.use("/tasks", taskRoutes);

// Server running confirmation
app.listen(port, () => console.log(`Server running at port: ${port}`));